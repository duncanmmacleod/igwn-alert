# Import testing modules:
import pytest
import os

# Import igwn-alert modules:

from igwn_alert import client as test_client

hop_auth_toml = """[[auth]]
username = "user-asdf"
password = "pw123"
"""

auth_netrc = "machine kafka://kafka.scimma.org/ login user-asdf password pw123"


def callback(topic=None, payload=None):
    return "This is a {0} from {1}".format(payload, topic)


# Test the version number
def test_version():
    from igwn_alert import __version__
    assert __version__ == '0.1.0'


# Test an unauthorized session
def test_no_auth():
    tc = test_client(noauth=True)
    assert tc.auth is None


USER_PASS_TEST_DATA = [
    {'username': 'user-asdf', 'password': 'pw123'},
    {'username': 'user-asdf'},
    {'password': 'pw123'},
]


# Test that username/password inputs work:
@pytest.mark.parametrize("username_and_or_password", USER_PASS_TEST_DATA)
def test_username_and_or_password_provided(username_and_or_password):

    # When there is not a username and a password, then the code should
    # produce an error. Define the error string and the error raised.

    if not len(username_and_or_password) == 2:
        err_str = 'You must provide both a username and a password for '\
                  'basic authentication.'
        with pytest.raises(RuntimeError, match=err_str):
            tc = test_client(**username_and_or_password)
    else:
        tc = test_client(**username_and_or_password)
        creds = tc.auth_obj

        assert creds.username == username_and_or_password.get('username')
        assert creds.password == username_and_or_password.get('password')


# Test for response when toml does not exist:
def test_hop_toml_not_exist():

    hop_toml = '/tmp/auth.toml'

    if os.path.exists(hop_toml):
        os.remove(hop_toml)

    with pytest.raises(FileNotFoundError):
        test_client(authfile=hop_toml)


# Test for response when toml exists but has bad permissions:
def test_hop_toml_bad_permissions():

    hop_toml = '/tmp/auth.toml'

    if os.path.exists(hop_toml):
        os.remove(hop_toml)

    with os.fdopen(os.open(hop_toml,
                   os.O_WRONLY | os.O_CREAT, 0o755), 'w') as h:
        h.write(hop_auth_toml)

    with pytest.raises(RuntimeError):
        test_client(authfile=hop_toml)


# Test for response when toml exists and has correct permissions:
def test_hop_toml_correct_permissions():

    hop_toml = '/tmp/auth.toml'

    if os.path.exists(hop_toml):
        os.remove(hop_toml)

    with os.fdopen(os.open(hop_toml,
                   os.O_WRONLY | os.O_CREAT, 0o600), 'w') as h:
        h.write(hop_auth_toml)

    tc = test_client(authfile=hop_toml)
    creds = tc.auth_obj[0]

    assert creds.username == 'user-asdf'
    assert creds.password == 'pw123'


# Test netrc file:
@pytest.mark.skip(reason="can't test noninvasively")
def test_netrc_auth():
    pass


# Test base URL formation:
def test_base_url():

    tc = test_client(noauth=True,
                     server='kafka://test.server',
                     port=1234,
                     group='test')

    assert tc._construct_base_url() == 'kafka://test.server:1234/test'


TOPICS_LIST = ['topic1',
               ['topic1'],
               ['topic1', 'topic2'],
               ]


# Test "topics" url with one or multiple topics:
@pytest.mark.parametrize("topics_item", TOPICS_LIST)
def test_topics_url(topics_item):

    tc = test_client(noauth=True,
                     server='kafka://test.server',
                     port=1234,
                     group='test')

    if (type(topics_item) == str):
        assert tc._construct_topic_url(topics_item) == \
            'kafka://test.server:1234/test.topic1'
    elif (type(topics_item) == list):
        if (len(topics_item) == 1):
            assert tc._construct_topic_url(topics_item) == \
                'kafka://test.server:1234/test.topic1'
        elif (len(topics_item) == 2):
            assert tc._construct_topic_url(topics_item) == \
                'kafka://test.server:1234/test.topic1,test.topic2'
