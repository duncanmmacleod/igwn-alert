.. Copyright (C) Alexander Pace (2021)
.. Copyright (C) Duncan Meacher (2021)

igwn-alert Documentation
============================

igwn-alert is a client for the LIGO/Virgo/KAGRA LVAlert pubsub infrastructure 
that is powered by `Apache Kafka`_. It is a version of the generalized
`hop-client`_ pubsub tool to respond to alerts from GraceDB. 

The CLI and API are compatible with Python 3.

User Guide
----------

Please visit the `igwn-alert User Guide`_ for a brief overview and starter for the LVAlert 
client and service. 

Quick Start
-----------

Install with pip_::

    pip install igwn-alert

Subscribe to some topics. See `User Guide on topics and credentials`_

Listen for  messages::

    igwn-alert listen

API
---

.. automodule:: igwn_alert.client
   :members:

Command Line Interface
----------------------

.. argparse::
    :module: igwn_alert.tool
    :func: parser
.. _Apache Kafka: https://kafka.apache.org/
.. _hop-client: https://hop-client.readthedocs.io/
.. _pip: http://pip.pypa.io/
.. _User Guide on topics and credentials: guide.html#managing-credentials-and-topics
.. _igwn-alert User Guide: guide.html
